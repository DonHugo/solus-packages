# Poor man's Solus User Repository

A collection of package definitions for Solus. Not endorsed or supported by their developers!
Use on your own risk, things might break!

## Requirements

Create file `/etc/solbuild/local-main-x86_64.profile` with following content

``` sh
#
# local-main-x86_64 configuration
#
# Build Solus packages using the main repository image.

image = "main-x86_64"

# If you have a local repo providing packages that exist in the main
# repository already, you should remove the repo, and re-add it *after*
# your local repository:
remove_repos = ['Solus']
add_repos = ['Local','Solus']

# A local repo with automatic indexing
[repo.Local]
uri = "/var/lib/solbuild/local"
local = true
autoindex = true

# Re-add the Solus stable repo
[repo.Solus]
uri = "https://mirrors.rit.edu/solus/packages/shannon/eopkg-index.xml.xz"
```

Setup everything from 
- https://getsol.us/articles/packaging/building-a-package/en/
    - `common` should be in src
- https://getsol.us/articles/packaging/local-repository/en
    - `sudo eopkg ar Solus https://mirrors.rit.edu/solus/packages/shannon/eopkg-index.xml.xz` to use stable repo

## CLR

Pre-compiled version from https://github.com/silkeh/clr-boot-manager/tree/add-mount-root

Supports btrfs boot and has option to mount boot (relevant for fwupd)

## Usage

- Copy desired packages from series.template into series
- Kernel modules must be in modules

Run `solero up` to build packages, update from Solus repos and rebuild kernel modules

## Good to know

- Using `linux-tkg` causes an issue with `cpu-powersave.service`, which changes the governor from
  `performance` to `ondemand` after boot
  - `sudo cp /usr/lib/systemd/system/cpu-powersave.service /etc/systemd/system/`
  - Comment `ConditionVirtualization=no`
