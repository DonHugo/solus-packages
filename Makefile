SHELL := /bin/bash
PKGS_REPO := /var/lib/solbuild/local/
PKGS_DIR := $(CURDIR)

.PHONY: clean
clean:
	@ ( \
		sudo rm -rfv $(PKGS_REPO) && \
		find src -type f \( -name \*.eopkg -o -name pspec_\*.xml \) -print -delete; \
	);

.PHONY: prepare
prepare:
	@ ( \
		sudo mkdir -p $(PKGS_REPO) && \
		echo Updating repository... && \
		git pull --ff-only --recurse-submodules && \
		echo Updating solbuild images... && \
		sudo solbuild update; \
	);

.PHONY: packages
packages: prepare
	@ ( \
		cd $(PKGS_REPO) && \
		for pkg_name in $$(grep -v ^# $(CURDIR)/src/series); do \
			pkg="$$(find $(CURDIR)/src -type d -name $${pkg_name})"; \
			if [ -z "$${pkg}" ]; then \
				echo "Package $$(tput bold)$${pkg_name}$$(tput sgr0) sources not found."; \
				continue; \
			fi; \
			pkg_version="$$(awk -F': ' '/^version/ {print $$2}' $${pkg}/package.yml)"; \
			pkg_release="$$(awk -F': ' '/^release/ {print $$2}' $${pkg}/package.yml)"; \
			if [ -f "$${pkg_name}-$${pkg_version}-$${pkg_release}-"*".eopkg" ]; then \
				echo "Package $$(tput bold)$${pkg_name}$$(tput sgr0) already in and up to date."; \
			else \
				for pkg_file in "$$(find $(PKGS_REPO) -type f -name "$${pkg_name}-*")"; do \
					echo "Removing Package $$(tput bold)$${pkg_file}$$(tput sgr0)."; \
					sudo rm -fv $${pkg_file}; \
				done; \
				echo "Compiling $$(tput bold)$${pkg_name}$$(tput sgr0)..."; \
				echo Triggering index...; \
				sudo eopkg index --skip-signing $(PKGS_REPO); \
				sudo solbuild build "$${pkg}/package.yml" -p local-main-x86_64 || \
				echo "$$(tput bold)Unable to build $${pkg_name}!$$(tput sgr0)"; \
			fi; \
		done; \
		sudo rm -fv *.xml; \
		echo Triggering index...; \
		sudo eopkg index --skip-signing $(PKGS_REPO); \
		sudo eopkg ur; \
		echo Finished.; \
	);

.PHONY: check
check:
	@ ( \
		find src -type f -name 'package.yml' | sort -u | while read -r pkg; do \
			pkg_name="$$(awk -F': ' '/^name/ {print $$2}' < "$${pkg}")"; \
			pkg_version="$$(awk -F': ' '/^version/ {print $$2}' < "$${pkg}")"; \
			pkg_src="$$(awk '/^\s+/ {print $$2}' < "$${pkg}" | head -1)"; \
			pkg_version_new="$$(cuppa q "$${pkg_src}" | awk '{print $$1}')"; \
			[ "$${pkg_version}" != "$${pkg_version_new}" ] && \
				[ "$${pkg_version_new}" != "🕱" ] && \
				echo "$${pkg_name}: $${pkg_version_new}" || echo -n; \
		done; \
	);

.PHONY: kernel-modules
kernel-modules:
	@ ( \
		cd $(PKGS_REPO) && \
		if [ -f "$(CURDIR)/src/modules" ]; then \
			for pkg_name in $$(grep -v ^# $(CURDIR)/src/modules); do \
				pkg="$$(find $(CURDIR)/src -type d -name $${pkg_name})"; \
					if [ -z "$${pkg}" ]; then \
						echo "Package $$(tput bold)$${pkg_name}$$(tput sgr0) sources not found."; \
						continue; \
					fi; \
				pkg_version="$$(awk -F': ' '/^version/ {print $$2}' $${pkg}/package.yml)"; \
				pkg_release="$$(awk -F': ' '/^release/ {print $$2}' $${pkg}/package.yml)"; \
				for pkg_file in "$$(find $(PKGS_REPO) -type f -name "$${pkg_name}*")"; do \
					sudo rm -fv $${pkg_file}; \
				done; \
				echo "Compiling $$(tput bold)$${pkg_name}$$(tput sgr0)..."; \
				echo Triggering index...; \
				sudo eopkg index --skip-signing $(PKGS_REPO); \
				sudo solbuild build "$${pkg}/package.yml" -p local-main-x86_64 && \
				sudo eopkg index --skip-signing $(PKGS_REPO); \
				sudo eopkg ur; \
				sudo eopkg it --reinstall $${pkg_name} && \
				sudo eopkg it --reinstall $${pkg_name}-current || \
				echo "$$(tput bold)Unable to build $${pkg_name}!$$(tput sgr0)"; \
			done; \
			sudo rm -fv *.xml; \
			echo Triggering index...; \
			sudo eopkg index --skip-signing $(PKGS_REPO); \
			echo Finished.; \
		else \
			echo No kernel modules specified.; \
		fi; \
	);
