#!/usr/bin/env bash

# Any subsequent commands which fail will cause the shell script to exit immediately
set -e

# Source library
source "$(dirname $(realpath ${BASH_SOURCE[0]}))/libsolero"

#
## Help
function _help {
    printf "Usage: solero [CMD] [OPTIONS] \n\n"
    printf "Update packages from Solus and local ones too\n"
    printf "CMD:\n"
    printf "  up / update     Update system. Updates local packages, Solus packages and rebuild kernel modules.\n"
    printf "  cleanup-kernels Cleanup installed kernels and only keep installed version.\n"
    printf "Options:\n"
    printf "  --only-update   Only update local packages.\n"
    printf "  --only-kernel   Only update custom kernel.\n"
    printf "  --only-modules  Only update kernel modules.\n"
    printf "  --skip-update   Skip updating local packages.\n"
    printf "  --skip-kernel   Skip updating custom kernel.\n"
    printf "  --skip-modules  Skip updating kernel modules.\n"
}

# If nothing is provided, print help and exit
if [ $# -eq 0 ]; then
    printf "No arguments provided\n"
    _help
    exit 1
fi


# Parse options
POSITIONAL=()
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        up|update)
            _UPDATE=1
            shift # past argument
            ;;
        cleanup-kernels)
            _CLEAN_KERNELS=1
            shift # past argument
            ;;
        --only-update)
            _ONLY_UPDATE=1
            shift # past argument
            ;;
        --only-kernel)
            _ONLY_KERNEL=1
            shift # past argument
            ;;
        --only-modules)
            _ONLY_MODULES=1
            shift # past argument
            ;;
        --skip-update)
            _SKIP_UPDATE=1
            shift # past argument
            ;;
        --skip-kernel)
            _SKIP_KERNEL=1
            shift # past argument
            ;;
        --skip-modules)
            _SKIP_MODULES=1
            shift # past argument
            ;;
        *)  # unknown option
            POSITIONAL+=("$1") # save it in an array for later
            shift # past argument
            ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

_ONLY_UPDATE="${_ONLY_UPDATE:-0}"
_ONLY_KERNEL="${_ONLY_KERNEL:-0}"
_ONLY_MODULES="${_ONLY_MODULES:-0}"

_SKIP_UPDATE="${_SKIP_UPDATE:-0}"
_SKIP_KERNEL="${_SKIP_KERNEL:-0}"
_SKIP_MODULES="${_SKIP_MODULES:-0}"

DEBUG="${DEBUG:-0}"


# Cleanup kernels
if [[ ! -z "$_CLEAN_KERNELS" ]]; then
    soleroCleanupKernels || exit 1
fi


# Run update
if [[ ! -z "$_UPDATE" ]]; then
    if [[ ! "$DEBUG" -eq 1 ]]; then
        soleroPrepare || exit 1
    fi
    if [[ "$_ONLY_UPDATE" -eq 1 ]]; then
        echo-info "Updating local packages ..."
        soleroPackages || exit 1
        exit 0
    fi
    if [[ "$_ONLY_KERNEL" -eq 1 ]]; then
        echo-info "Updating custom kernels ..."
        soleroKernels || exit 1
        exit 0
    fi
    if [[ "$_ONLY_MODULES" -eq 1 ]]; then
        echo-info "Rebuilding kernel modules ..."
        soleroModules || exit 1
        soleroReinstallModules || exit 1
        exit 0
    fi

    if [[ ! "$_SKIP_UPDATE" -eq 1 ]]; then
        echo-info "Updating local packages ..."
        soleroPackages || exit 1
        sleep 1
    fi
    if [[ ! "$_SKIP_KERNEL" -eq 1 ]]; then
        echo-info "Updating custom kernels ..."
        soleroKernels || exit 1
        sleep 1
    fi
    if [[ ! "$_SKIP_MODULES" -eq 1 ]]; then
        echo-info "Rebuilding kernel modules ..."
        soleroModules || exit 1
        sleep 1
    fi
    echo-info "Updating Solus packages ..."
    sudo eopkg up || exit 1
    if [[ ! "$_SKIP_MODULES" -eq 1 ]]; then
        sleep 1
        echo-info "Reinstalling kernel modules ..."
        soleroReinstallModules || exit 1
    fi
    
    exit 0
fi
